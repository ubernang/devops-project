provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "spe-rg" {
  name     = "spe-rg"
  location = "West Europe"
}

resource "azurerm_virtual_network" "spe-vnet" {
  name                = "spe-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.spe-rg.location
  resource_group_name = azurerm_resource_group.spe-rg.name
}

resource "azurerm_subnet" "spe-subnet" {
  name                 = "spe-subnet"
  resource_group_name  = azurerm_resource_group.spe-rg.name
  virtual_network_name = azurerm_virtual_network.spe-vnet.name
  address_prefix       = "10.0.2.0/24"
}


resource "azurerm_public_ip" "spe-pip" {
  name                    = "spe-pip"
  location                = azurerm_resource_group.spe-rg.location
  resource_group_name     = azurerm_resource_group.spe-rg.name
  allocation_method       = "Dynamic"
  idle_timeout_in_minutes = 30

}

resource "azurerm_network_interface" "spe-nic" {
  name                = "spe-nic"
  location            = azurerm_resource_group.spe-rg.location
  resource_group_name = azurerm_resource_group.spe-rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.spe-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.spe-pip.id
  }
}

resource "azurerm_linux_virtual_machine" "spe-vm" {
  name                            = "spe-vm"
  resource_group_name             = azurerm_resource_group.spe-rg.name
  location                        = azurerm_resource_group.spe-rg.location
  size                            = "Standard_F2"
  admin_username                  = "adminuser"
  admin_password                  = "Password1234!"
  disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.spe-nic.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
}